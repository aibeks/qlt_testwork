from flask import Blueprint, jsonify

from server.db import Status
from ..db import get_db

bp = Blueprint('status', __name__, url_prefix='/status')


@bp.route('/list')
def all_status():
    db = get_db()
    statuses = db.session.query(Status).all()
    statuses = [s.serialize for s in statuses]
    return jsonify(statuses=statuses)
