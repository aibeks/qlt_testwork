import {SERVER_API} from "../Consts";
import {parseJSON} from "./WebApi";

export const listStatuses = () => {
    return fetch(SERVER_API + `status/list`).then(parseJSON).then(res => res.statuses)
}