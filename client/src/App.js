import React from 'react';
import './App.css';
import './components/Layout'
import Layout from "./components/Layout";
import DatePickerComponent from "./components/DatePickerComponent";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedDate: new Date()
        }
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    handleDateChange(selectedDate) {
        this.setState({
            selectedDate
        })
    }

    render() {
        return (
            <div style={{width: '800px', margin: '0 auto'}}>
                <h1 style={{textAlign: 'center'}}>Booking App</h1>
                <DatePickerComponent handleDateChange={this.handleDateChange}/>
                <Layout currentDate={this.state.selectedDate}/>
            </div>
        );
    }

}

export default App;
