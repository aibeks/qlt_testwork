import React from 'react';
import {Modal, Button, Form} from 'react-bootstrap'
import * as WebApi from '../apis/WebApi'
import {Formik} from "formik";
import * as yup from 'yup';


class AddBookingModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            statuses: [],
            selectedStatus: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleStatusChange = this.handleStatusChange.bind(this)
    }

    componentDidMount() {
        WebApi.listStatuses().then(statuses => {
            this.setState({
                statuses,
                selectedStatus: statuses[0].name
            })
            this.schema = yup.object({
                fullName: yup.string().required(),
                phone: yup.string().required(),
                service: yup.string().required(),
                comments: yup.string()
            });
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.initialValues.status !== this.props.initialValues.status || this.state.selectedStatus === '' && this.props.initialValues.status !== '') {
            this.setState({
                selectedStatus: this.props.initialValues.status
            })
        }
    }

    handleSubmit(e) {
        const statusId = this.state.selectedStatus === '' ? 1 : this.state.statuses.find(status => status.name === this.state.selectedStatus).id
        const bookingId = this.props.booking ? this.props.booking.id : null;
        this.props.handleCreate({
            fullName: e.fullName,
            phone: e.phone,
            service: e.service,
            comments: e.comments,
            statusId,
            bookingId
        })
        this.setState({
            selectedStatus: ''
        })
    }

    handleStatusChange(e) {
        this.setState({
            selectedStatus: e.target.value
        })
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.editing ? 'Update' : 'Add'} Booking</Modal.Title>
                </Modal.Header>

                <Formik
                    validationSchema={this.schema}
                    onSubmit={this.handleSubmit}
                    initialValues={this.props.initialValues}
                >
                    {props => {
                        const {
                            handleSubmit,
                            handleChange,
                            values,
                            touched,
                            errors
                        } = props
                        return (
                            <div>
                                <Modal.Body>
                                    <Form noValidate onSubmit={handleSubmit}>
                                        <Form.Group controlId="addBooking.fullName">
                                            <Form.Label>Full Name</Form.Label>
                                            <Form.Control
                                                name="fullName"
                                                value={values.fullName}
                                                onChange={handleChange}
                                                isValid={touched.fullName && !errors.fullName}
                                                isInvalid={!!errors.fullName}
                                                type="text" placeholder="Enter full name"/>
                                            <Form.Control.Feedback>Correct</Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="addBooking.phone">
                                            <Form.Label>Phone No</Form.Label>
                                            <Form.Control
                                                name="phone"
                                                value={values.phone}
                                                onChange={handleChange}
                                                isValid={touched.phone && !errors.phone}
                                                isInvalid={!!errors.phone}
                                                type="text" placeholder="Enter phone number"/>
                                            <Form.Control.Feedback>Correct</Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="addBooking.service">
                                            <Form.Label>Service</Form.Label>
                                            <Form.Control
                                                name="service"
                                                value={values.service}
                                                onChange={handleChange}
                                                isValid={touched.service && !errors.service}
                                                isInvalid={!!errors.service}
                                                type="text" placeholder="Enter service"/>
                                            <Form.Control.Feedback>Correct</Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="addBooking.comments">
                                            <Form.Label>Comments</Form.Label>
                                            <Form.Control
                                                name="comments"
                                                value={values.comments}
                                                onChange={handleChange}
                                                isValid={touched.comments && !errors.comments}
                                                isInvalid={!!errors.comments}
                                                type="text" placeholder="Enter comments"/>
                                            <Form.Control.Feedback>Correct</Form.Control.Feedback>
                                        </Form.Group>
                                        <Form.Group controlId="addBooking.status">
                                            <Form.Label>Status</Form.Label>
                                            <Form.Control
                                                as="select"
                                                name="status"
                                                value={this.state.selectedStatus}
                                                onChange={this.handleStatusChange}
                                            >
                                                {this.state.statuses && this.state.statuses.map(status => {
                                                    return (
                                                        <option
                                                            key={status.id}>{status.name}</option>)
                                                })}
                                            </Form.Control>
                                        </Form.Group>
                                    </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                    {this.props.editing && <Button variant="danger" onClick={() => this.props.handleRemove(this.props.booking.id)}>
                                        Remove
                                    </Button> }
                                    <Button variant="secondary" onClick={this.props.handleClose}>
                                        Close
                                    </Button>
                                    <Button variant="primary" onClick={handleSubmit}>
                                        {this.props.editing ? 'Update' : 'Create'}
                                    </Button>
                                </Modal.Footer>
                            </div>
                        )
                    }}
                </Formik>
            </Modal>
        );
    }

}

export default AddBookingModal;
