Create Python virtual environment, and activate it

Install Flask:

```pip install Flask```

Install dependencies:

```pip install -r requirements.txt```

To initialize the database:

```python -m server.init_db```

Run in the shell:
```
export FLASK_ENV=development
export FLASK_APP=server.app
flask run
```
