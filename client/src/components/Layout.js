import React from 'react';
import {Button, Table} from 'react-bootstrap';
import AddBookingModal from "./AddBookingModal";
import * as WebApi from '../apis/WebApi'
import moment from "moment/min/moment-with-locales";
import BookingRow from "./BookingRow";

class Layout extends React.Component {

    constructor(props) {
        super(props);
        this.times = []
        for (let i = 0; i < 24; i++) {
            let time = '';
            if (i < 10) {
                time += '0';
            }
            time += i + ':00';
            this.times.push(time)
        }
        this.state = {
            showModal: false,
            selectedTime: '',
            bookings: [],
            bookingsMap: {},
            editing: false,
            selectedBooking: null,
            initialValues: {}
        }
        this.clickTime = this.clickTime.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleCreate = this.handleCreate.bind(this)
        this.fetchBookings = this.fetchBookings.bind(this)
        this.clickBooking = this.clickBooking.bind(this)
        this.resetModal = this.resetModal.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
    }

    fetchBookings() {
        const fromDate = new Date(this.props.currentDate);
        fromDate.setHours(0, 0);
        const toDate = new Date(this.props.currentDate);
        toDate.setHours(23, 59);
        WebApi.listBookings({
            fromDate: fromDate.toISOString(),
            toDate: toDate.toISOString()
        }).then(bookings => {
            bookings = bookings.map(b => {
                b = JSON.parse(JSON.stringify(b))
                b.date = moment.utc(b.date).local().toDate()
                return b
            })
            const bookingsMap = {}
            this.times.map(t => {
                bookingsMap[t] = []
            })
            bookings.forEach(b => {
                let t = ''
                if (b.date.getHours() < 10) {
                    t += '0'
                }
                t += b.date.getHours() + ':00';
                bookingsMap[t].push(b)
            })
            this.setState({
                bookingsMap
            })
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.currentDate !== this.props.currentDate) {
            this.fetchBookings()
        }
    }

    componentDidMount() {
        this.fetchBookings()
    }

    clickTime(selectedTime) {
        this.setState({
            showModal: true,
            selectedTime,
            initialValues: {
                fullName: '',
                phone: '',
                service: '',
                comments: '',
                status: ''
            }
        })
    }

    clickBooking(bookingId) {
        WebApi.getBooking(bookingId).then(booking => {
            this.setState({
                selectedBooking: booking,
                initialValues: {
                    fullName: booking.full_name,
                    phone: booking.phone,
                    service: booking.service,
                    comments: booking.comments,
                    status: booking.status
                },
                editing: true,
                showModal: true
            })
        })
    }

    resetModal() {
        this.setState({
            showModal: false,
            selectedTime: '',
            editing: false,
            selectedBooking: null
        })
    }

    handleCloseModal() {
        this.resetModal()
    }

    handleRemove(bookingId) {
        WebApi.removeBooking(bookingId).then(() => {
            this.fetchBookings()
        }).catch(err => {
            console.log('Error deleting booking ' + err)
        })
        this.resetModal()
    }


    handleCreate(fields) {
        if (this.state.editing && fields.bookingId) {
            WebApi.updateBooking(fields).then(() => {
                this.fetchBookings()
            }).catch(err => {
                console.log('Error updating booking ' + err)
            })
        } else {
            const date = new Date(this.props.currentDate);
            const t = this.state.selectedTime.split(':')
            date.setHours(parseInt(t[0]), 0)
            fields = {
                date: date.toISOString(),
                ...fields
            }
            WebApi.addBooking(fields).then(() => {
                this.fetchBookings()
            }).catch(err => {
                console.log('Error creating booking ' + err)
            })
        }
        this.resetModal()
    }

    render() {
        return (
            <div style={{marginTop: 30, width: '800px'}}>
                <AddBookingModal editing={this.state.editing}
                                 booking={this.state.selectedBooking}
                                 initialValues={this.state.initialValues}
                                 handleCreate={this.handleCreate}
                                 handleRemove={this.handleRemove}
                                 show={this.state.showModal}
                                 handleClose={this.handleCloseModal}/>
                <Table striped bordered hover size="sm" style={{width: '100%'}}>
                    <thead>
                    <tr>
                        <th>Time</th>
                        <th>Bookings</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.times && this.times.map((time, index) => {
                        return (
                            <tr key={index}>
                                <td style={{cursor: 'pointer'}}
                                    onClick={() => this.clickTime(time)}>{time}</td>
                                <td>
                                    {this.state.bookingsMap && this.state.bookingsMap[time] &&
                                    <BookingRow clickBooking={this.clickBooking}
                                                bookings={this.state.bookingsMap[time]}/>}
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default Layout;
