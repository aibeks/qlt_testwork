from .db import get_db
from .db import Base, Status

session = get_db().session

Base.metadata.create_all()

c = Status(name='new', color='green')
session.add(c)
session.commit()

c = Status(name='booked', color='blue')
session.add(c)
session.commit()

c = Status(name='not sure', color='orange')
session.add(c)
session.commit()

c = Status(name='done', color='violet')
session.add(c)
session.commit()