from flask import Flask
from flask_cors import CORS

from .db import init_db

db = None

app = Flask(__name__)
init_db(app)

CORS(app, resources={r"/*": {"origins": "*"}})

from .blueprints import status
from .blueprints import booking

app.register_blueprint(status.bp)
app.register_blueprint(booking.bp)


@app.route('/')
def hello():
    return 'Hello, World!'