import React from 'react';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import {
    DatePicker,
} from '@material-ui/pickers';

class DatePickerComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDate: new Date()
        }
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    handleDateChange(e) {
        this.setState({
            selectedDate: e
        })
        this.props.handleDateChange(e);
    }

    render() {
        return (
            <div style={{display: 'flex', width: '800px', justifyContent: 'center', marginTop: 30}}>
                <div style={{
                    fontFamily: 'Roboto',
                    display: 'inline',
                    marginLeft: 50,
                    marginTop: 5
                }}>
                    Select Date
                </div>
                <div style={{display: 'inline', marginLeft: 50}}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DatePicker value={this.state.selectedDate}
                                    onChange={this.handleDateChange}/>
                    </MuiPickersUtilsProvider>
                </div>
            </div>
        );
    }

}

export default DatePickerComponent;
