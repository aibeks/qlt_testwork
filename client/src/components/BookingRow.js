import React from 'react';

class BookingRow extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div style={{
                overflow: 'auto',
                whiteSpace: 'nowrap',
                width: '700px',
                height: '50px'
            }}>
                {
                    this.props.bookings.map(booking => {
                        return (
                            <div key={booking.id} style={{
                                backgroundColor: booking.color,
                                display: 'inline-block',
                                color: 'white',
                                marginLeft: 10,
                                marginRight: 10,
                                borderRadius: '25px',
                                border: '2px solid ' + booking.color,
                                paddingLeft: 5,
                                paddingRight: 5,
                                cursor: 'pointer'
                            }} onClick={() => this.props.clickBooking(booking.id)}>
                                {booking.service} - {booking.full_name}
                            </div>)
                    })
                }
            </div>
        );
    }

}

export default BookingRow;
