import {SERVER_API} from "../Consts";
import {parseJSON} from "./WebApi";

export const listBookings = ({fromDate, toDate}) => {
    return fetch(SERVER_API + `booking/list?fromDate=${fromDate}&toDate=${toDate}`).then(parseJSON).then(res => res.bookings)
}

export const addBooking = ({date, fullName, phone, service, comments, statusId}) => {
    return fetch(SERVER_API + `booking/`, {
        method: 'POST',
        body: JSON.stringify({
            date,
            fullName,
            phone,
            service,
            comments,
            statusId
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(parseJSON)
}

export const getBooking = (bookingId) => {
    return fetch(SERVER_API + `booking/${bookingId}`).then(parseJSON).then(res => res.booking)
}

export const updateBooking = ({fullName, phone, service, comments, statusId, bookingId}) => {
    return fetch(SERVER_API + `booking/${bookingId}`, {
        method: 'PATCH',
        body: JSON.stringify({
            fullName,
            phone,
            service,
            comments,
            statusId
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(parseJSON)
}

export const removeBooking = (bookingId) => {
     return fetch(SERVER_API + `booking/${bookingId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(parseJSON)
}
