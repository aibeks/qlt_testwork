export const SERVER_API = (process.env.NODE_ENV === 'production' ?
    "REMOTE_SERVER_HOST:5000/" : "http://localhost:5000/")