from sqlalchemy import Column, DateTime, String, Integer, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy

Base = declarative_base()


class Status(Base):
    __tablename__ = 'status'

    id = Column(Integer, primary_key=True)
    name = Column(String(120), nullable=False, unique=True)
    color = Column(String(120), nullable=False, unique=True)

    def __repr__(self):
        return '<Status %r>' % self.name

    @property
    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'color': self.color
        }


class Booking(Base):
    __tablename__ = 'booking'

    id = Column(Integer, primary_key=True)
    date = Column(DateTime, nullable=False)
    full_name = Column(String(120), nullable=False)
    phone = Column(String(120), nullable=False)
    service = Column(String(120), nullable=False)
    comments = Column(String(120), nullable=True)
    status_id = Column(
        Integer,
        ForeignKey('status.id', ondelete='CASCADE'),
        nullable=False)
    status = relationship(Status, lazy='joined')

    def __repr__(self):
        return '<Booking %r %r>' % (self.date, self.full_name)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'date': self.date.isoformat(),
            'full_name': self.full_name,
            'phone': self.phone,
            'service': self.service,
            'comments': self.comments,
            'status': self.status.name,
            'color': self.status.color
        }


db = None


def init_db(app):
    global db
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db = SQLAlchemy(app)
    Base.metadata.bind = db.engine
    print('DB is initialized')


def get_db():
    global db
    if db is None:
        from .app import app
    return db
