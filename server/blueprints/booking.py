import datetime
import dateutil.parser

from flask import Blueprint, jsonify, request
from sqlalchemy import and_

from server.db import Booking
from ..db import get_db

bp = Blueprint('booking', __name__, url_prefix='/booking')


@bp.route('/list')
def all_booking():
    from_date = request.args['fromDate']
    from_date = dateutil.parser.isoparse(from_date)
    to_date = request.args['toDate']
    to_date = dateutil.parser.isoparse(to_date)
    db = get_db()
    bookings = db.session.query(Booking).filter(
        and_(Booking.date >= from_date, Booking.date <= to_date))
    bookings = [b.serialize for b in bookings]
    return jsonify(bookings=bookings)


@bp.route('/', methods=['POST'])
def add_booking():
    date = request.json['date']
    date = dateutil.parser.isoparse(date)
    full_name = request.json['fullName']
    phone = request.json['phone']
    service = request.json['service']
    comments = request.json['comments']
    status_id = request.json['statusId']
    db = get_db()
    booking = Booking(date=date, full_name=full_name, phone=phone, service=service,
                      comments=comments, status_id=status_id)
    db.session.add(booking)
    db.session.commit()
    return jsonify(success=True)


@bp.route('/<int:id>', methods=['GET'])
def get_booking(id):
    db = get_db()
    booking = db.session.query(Booking).get(id).serialize
    return jsonify(booking=booking)


@bp.route('/<int:id>', methods=['DELETE'])
def delete_booking(id):
    db = get_db()
    booking = db.session.query(Booking).get(id)
    db.session.delete(booking)
    db.session.commit()
    return jsonify(success=True)


@bp.route('/<int:id>', methods=['PATCH'])
def update_booking(id):
    full_name = request.json.get('fullName')
    phone = request.json.get('phone')
    service = request.json.get('service')
    comments = request.json.get('comments')
    status_id = request.json.get('statusId')
    db = get_db()
    booking = db.session.query(Booking).get(id)
    print(booking)
    print(full_name, phone, service, comments, status_id)
    if full_name is not None and full_name != '':
        booking.full_name = full_name
    if phone is not None and phone != '':
        booking.phone = phone
    if service is not None and service != '':
        booking.service = service
    if comments is not None and comments != '':
        booking.comments = comments
    if status_id is not None:
        booking.status_id = status_id
    db.session.commit()
    return jsonify(success=True)
