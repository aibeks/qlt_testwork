export function parseJSON(res) {
    if (res.status >= 400) {
        return res.json().then(data => {
            const error = new Error("Server returned non OK status");
            error.status = res.status;
            error.data = data;
            return Promise.reject(error);
        });
    }
    const contentType = res.headers.get("content-type");
    if (contentType && contentType.includes("application/json")) {
        return res.json();
    }
    return res;
}

export * from './Booking'
export * from './Status'